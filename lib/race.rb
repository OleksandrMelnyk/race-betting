module RaceBet
  class Race

    class << self
      def score(guesses, winners)
        points = [15, 10, 5, 3, 1, 1, 1]
        guesses.zip(winners, points).map do |e|
          next if e[1].nil? || !e[0].is_a?(e[1].class) || e[1] == :loser
          e[0] == e[1] ? e[2] : 1
        end.compact.reduce(&:+) || 0
      end
    end

  end
end
